int main()
{
  /* Interupt vector address */
  int  *p = (int  *)0x00000020;

  *p = 0; /* Write offset in interrupt vector */
  p++;
  *p = 0xFFFF; /* Write segment in interrupt vector */
}

