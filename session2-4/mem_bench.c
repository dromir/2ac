#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>

int main(int argc, char* argv[]) {
  float* array;
  int num_elems;

  if (argc != 2) {
     fprintf(stderr, "Wrong number of arguments. Usage: %s num_elems\n",
         argv[0]);
     return -1;
  }

  // Read from the command line the number of elements
  num_elems = atoi(argv[1]);
  int bytes = num_elems * sizeof(float);

  if (num_elems > INT_MAX / 4) {
    printf("Maximum number of elements allowed: %d\n", INT_MAX / 4);
    return -1;
  }

  printf("Using %d elements\n", num_elems);
  printf("Used memory: %d bytes = %.2f KiBytes  = %.2f MiBytes\n",
    bytes, ((float) bytes) / 1024.0, ((float) bytes) / (1024.0 * 1024));

  // Allocate memory
  array = (float*) malloc(bytes);
  if (array == NULL) {
    fprintf(stderr, "Error allocating memory. Finishing\n");
    return -1;
  }

  // Initialize the random seed
  srand(time(NULL));

  // Write num_elems random positions
  for (int i = 0; i < num_elems; i++) {
    int random_pos = rand() % num_elems;
    array[random_pos] = rand();
  }

  return 0;
}
