#include <stdio.h>

int main()
{
    int i = 3;
    float f = 2.4;
    char * s = "Content";

    // Print the content of i
	printf("i content: %d", i);
    // Print the content of f
	printf("f content: %f", f);
    // Print the content of the string
	printf("s content: %s", s);

    return 0;
}
