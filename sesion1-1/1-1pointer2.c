#include <stdio.h>

void f(double * d)
{
   *d = 4;
}

int main(int argc, char* argv[])
{
    double dD=3;
    double* d=&dD;

    f(d);
    printf("%f\n", *d);
    return 0;
}
